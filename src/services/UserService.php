<?php
namespace App\services;


use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\String\Slugger\SluggerInterface;

class UserService
{
    protected $mailer;
    protected $slugger;


    public function __construct(MailerInterface $mailer, SluggerInterface $slugger)
    {
        $this->mailer = $mailer;
        $this->slugger = $slugger;

        // $this->imageManager = new ImageManager(['driver' => 'gd']);
    }


    /*
     * Mailer
     */
    public function emailNotify($email)
    {

        $email = (new Email())
            ->from('gatonegro2123@gmail.com ')
            ->to('info@enriqueteruel.com')
            ->subject('Account Created in Crypto App!')
            ->text('Login now!')
            ->html('<p>Datos del User!</p>');

        $this->mailer->send($email);

    }

    public function uploadImage(UploadedFile $file, $dest)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        try {
            $file->move($dest, $fileName);

        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $fileName;
    }

}