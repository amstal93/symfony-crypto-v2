<?php

namespace App\Controller;


use App\Entity\User;
use App\Entity\Wallet;
use App\Form\UserType;
use App\services\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{


    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    #[Route('user/new', name: 'new_user')]
    public function addUser(Request $request, EntityManagerInterface $doctrine, UserService $service): Response
    {

        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){


            $user= $form->getData();

            $userImage = $form->get('avatar')->getData();
            if ($userImage) {
                $userImageName = $service->uploadImage($userImage, $this->getParameter('kernel.project_dir').'/public/images');
                $user->setAvatar("/images/$userImageName");
            }

            $wallet = new Wallet();
            $wallet->setAmount(0);
            $wallet->setUserWallet($user);

            $user->setPassword($this->passwordHasher->hashPassword($user, $user->getPassword()));

            $doctrine->persist($user);
            $doctrine->persist($wallet);
            $doctrine->flush();

            $email = $user->getEmail();
            $service->emailNotify($email);

            $this->addFlash('success', 'Usuario creado correctamente');
            return $this->redirectToRoute('see_user',['id'=>$user->getId()]);
        }


        return $this->renderForm('user/newUser.html.twig', ['userForm'=>$form]);
    }

    #[Route('user/profile/{id}', name: 'see_user')]
    #[IsGranted("ROLE_USER")]
    public function searchUser(EntityManagerInterface $doctrine, $id): Response
    {

        $repo = $doctrine->getRepository(User::class);
        $user = $repo->find($id);
        return $this->render('user/userSearch.html.twig', [ 'user' => $user ]);
    }

    #[Route('user/delete/{id}', name: 'delete_user')]
    public function deleteUser(EntityManagerInterface $doctrine, $id): Response
    {

        $repo = $doctrine->getRepository(User::class);
        $user = $repo->find($id);
        $doctrine->remove($user);
        $doctrine->flush();
        $this->addFlash('success', 'Usuario Borrado correctamente');
        return $this->redirectToRoute('app_crypto');
    }

}
